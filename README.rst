===============================
Weighted_Histogram_Method
===============================

.. image:: https://img.shields.io/travis//nmr_fs19_cmse802.svg
        :target: https://travis-ci.org//nmr_fs19_cmse802

.. image:: https://img.shields.io/pypi/v/nmr_fs19_cmse802.svg
        :target: https://pypi.python.org/pypi/nmr_fs19_cmse802


Developing a nonequilibrium weighted histogram analysis method for pulling trajectories

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://.github.io/nmr_fs19_cmse802.

Features
--------

* TODO
