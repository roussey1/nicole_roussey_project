.. weighted_histogram_method documentation master file, created by
   sphinx-quickstart on Mon Oct 14 14:53:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to weighted_histogram_method's documentation!
=====================================================

.. toctree::
   :maxdepth: 1

   installation
   usage
   release-history
   min_versions
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
