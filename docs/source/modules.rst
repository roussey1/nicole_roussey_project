nicole_roussey_project
======================

.. toctree::
   :maxdepth: 4

   run_script
   setup
   tests
   versioneer
   weighted_histogram_method
